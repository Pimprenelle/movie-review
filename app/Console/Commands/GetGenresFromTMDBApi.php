<?php

namespace App\Console\Commands;

use App\Services\Genre\InitGenreService;
use Illuminate\Console\Command;

class GetGenresFromTMDBApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:get-genres';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch movie genres from TMDB API';

    /**
     * Execute the console command.
     */
    public function handle(InitGenreService $initGenreService)
    {
        // Initialisation des genres dans l'application
        $response = $initGenreService->createGenresFromApiIfNotExists();

        if($response){
            $this->info('Movie genres successfully fetched.');
        }else{
            $this->alert('Movie genres already fetched.');
        }

        return 0;
    }
}
