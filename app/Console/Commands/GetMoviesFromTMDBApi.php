<?php

namespace App\Console\Commands;

use App\Services\Genre\InitTrendingMovieService;
use App\Services\TMDB\TrendingMovieService;
use Illuminate\Console\Command;

class GetMoviesFromTMDBApi extends Command
{
    /**
     * @var string
     */
    protected $signature = 'api:get-movies {--d|day : Fetch trending movies of the day} {--w|week : Fetch trending movies of the week}';

    /**
     * @var string
     */
    protected $description = 'Fetch trending movies from TMDB API. By default trending movies of the day.';

    /**
     * Execute the console command.
     */
    public function handle(InitTrendingMovieService $initTrendingMovieService)
    {
        $timeWindow = $this->option('day')
            ? TrendingMovieService::TIME_DAY
            : ($this->option('week') ? TrendingMovieService::TIME_WEEK : TrendingMovieService::TIME_DAY);

        $this->info('Fetching trending movies of the ' . $timeWindow);

        // Init/Rafraichissement des films trendings (1 fois par jour max)
        $response = $initTrendingMovieService->refreshMoviesFromApi($timeWindow);

        if($response){
            $this->info('Trending movies of the ' . $timeWindow . ' successfully fetched.');
        }else{
            $this->alert('Trending movies of the ' . $timeWindow . ' already fetched today.');
        }

        return 0;
    }
}
