<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = [
        'adult',
        'backdrop_path',
        'tmdb_id',
        'title',
        'original_language',
        'original_title',
        'overview',
        'poster_path',
        'popularity',
        'release_date',
        'vote_average',
        'vote_count',
        'refresh_trending_day_date',
        'refresh_trending_week_date'
    ];

    protected $casts = [
        'adult' => 'boolean',
        'budget' => 'integer',
        'tmdb_id' => 'integer',
        'release_date' => 'date',
        'id' => 'integer',
        'popularity' => 'float',
        'revenue' => 'integer',
        'vote_average' => 'float',
        'vote_count' => 'integer',
        'refresh_trending_day_date' => 'date',
        'refresh_trending_week_date' => 'date'
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

}
