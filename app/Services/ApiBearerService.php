<?php

namespace App\Services;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class ApiBearerService
{
    /**
     * @var string|null
     */
    private ?string $apiKey;

    /**
     * @param string $apiKey
     * @return $this
     */
    public function setApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;
        return $this;
    }


    /**
     * @param array $headers
     * @param bool $authorizationInherited
     * @return array
     */
    public function generateHeaders(array $headers = [], bool $authorizationInherited = true): array
    {
        if (!$authorizationInherited) {
            return $headers;
        }

        return array_merge(['Authorization' => 'Bearer ' . $this->apiKey], $headers);
    }


    /**
     * @param string $url
     * @param array $parameters
     * @param array $headers
     * @param bool $authorizationInherited
     * @return Response
     */
    public function get(string $url, array $parameters = [], array $headers = [], bool $authorizationInherited = true): Response
    {
        return Http::withHeaders(
            $this->generateHeaders($headers, $authorizationInherited)
        )->get($url, $parameters);
    }


}
