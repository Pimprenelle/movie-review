<?php

namespace App\Services\TMDB;

use App\Services\ApiBearerService;

class MovieGenreService
{

    private $apiBaseUrl;
    private ApiBearerService $apiBearerService;

    public function __construct(ApiBearerService $apiBearerService)
    {
        $this->apiBaseUrl = config('services.tmdb.api_base_url');
        $this->apiBearerService = $apiBearerService->setApiKey(config('services.tmdb.api_key'));
    }

    public function getMovieGenres()
    {
        $response = $this->apiBearerService->get(
            url: $this->apiBaseUrl . "/genre/movie/list",
            parameters: ['language' => 'fr-FR']
        );
        return $response->json();
    }

}
