<?php

namespace App\Services\TMDB;

use App\Services\ApiBearerService;

class MovieDetailService
{

    private $apiBaseUrl;
    private ApiBearerService $apiBearerService;

    public function __construct(ApiBearerService $apiBearerService)
    {
        $this->apiBaseUrl = config('services.tmdb.api_base_url');
        $this->apiBearerService = $apiBearerService->setApiKey(config('services.tmdb.api_key'));
    }

    public function getMovieDetails(string $movieId)
    {

        $response = $this->apiBearerService->get(
            url: $this->apiBaseUrl . "/movie/$movieId",
            parameters: ['language' => 'fr-FR']
        );

        return $response->json();
    }

}
