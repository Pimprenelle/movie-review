<?php

namespace App\Services\TMDB;

use App\Services\ApiBearerService;

class TrendingMovieService
{

    public const TIME_DAY = 'day';
    public const TIME_WEEK = 'week';

    private $apiBaseUrl;
    private ApiBearerService $apiBearerService;

    public function __construct(ApiBearerService $apiBearerService)
    {
        $this->apiBaseUrl = config('services.tmdb.api_base_url');
        $this->apiBearerService = $apiBearerService->setApiKey(config('services.tmdb.api_key'));
    }

    public function getTrendingMovies(string $timeWindow)
    {
        $response = $this->apiBearerService->get(
            url: $this->apiBaseUrl . "/trending/movie/$timeWindow",
            parameters: ['language' => 'fr-FR']
        );
        return $response->json()['results'];
    }

}
