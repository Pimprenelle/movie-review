<?php

namespace App\Services\Genre;

use App\Models\Genre;
use App\Models\Movie;
use App\Services\TMDB\TrendingMovieService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class InitTrendingMovieService
{

    private TrendingMovieService $trendingMovieService;

    public function __construct(TrendingMovieService $trendingMovieService)
    {
        $this->trendingMovieService = $trendingMovieService;
    }


    /**
     * Fonction qui permet de rafraichir la liste des films trendings
     * @param string $timeWindow
     * @return bool
     */
    public function refreshMoviesFromApi(string $timeWindow): bool
    {
        if (!$this->checkLastRefreshInDatabase($timeWindow)) {
            $resultMovies = $this->trendingMovieService->getTrendingMovies($timeWindow);

            foreach ($resultMovies as $resultMovie) {

                // On garde l'id de tmdb pour les associations
                $resultMovie['tmdb_id'] = $resultMovie['id'];
                unset($resultMovie['id']);

                $movie = Movie::updateOrCreate(
                    ['tmdb_id' => $resultMovie['tmdb_id']],
                    $resultMovie
                );

                // On rafraichit la date de rafraichissement.
                $movie->{'refresh_trending_' . $timeWindow . '_date'} = Carbon::today();

                // On gère l'association aux genres
                $genres = Genre::whereIn('tmdb_id', $resultMovie['genre_ids'])->get();
                $movie->genres()->syncWithoutDetaching($genres);

                $movie->save();
            }

            return true;
        }

        return false;
    }


    public function checkLastRefreshInDatabase(string $timeWindow): bool
    {
        $today = Carbon::today();
        $count = DB::table('movies')
            ->whereDate("refresh_trending_" . $timeWindow . "_date", $today)
            ->count();

        return $count > 0;
    }
}
