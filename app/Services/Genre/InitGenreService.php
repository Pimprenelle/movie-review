<?php

namespace App\Services\Genre;

use App\Models\Genre;
use App\Services\TMDB\MovieGenreService;

class InitGenreService
{

    private MovieGenreService $movieGenreService;

    public function __construct(MovieGenreService $movieGenreService)
    {
        $this->movieGenreService = $movieGenreService;
    }

    /**
     * Permet d'initiamiser les genres de TMDB dans l'application
     * @return bool
     */
    public function createGenresFromApiIfNotExists(): bool
    {
        if (!$this->checkIfGenresExistInDatabase()) {
            $resultGenres = $this->movieGenreService->getMovieGenres();

            foreach ($resultGenres['genres'] as $resultGenre) {

                // On garde l'id de tmdb pour les associations
                $resultGenre['tmdb_id'] = $resultGenre['id'];
                unset($resultGenre['id']);

                Genre::updateOrCreate(
                    ['tmdb_id' => $resultGenre['tmdb_id']],
                    $resultGenre
                );
            }
            return true;
        }

        return false;
    }


    public function checkIfGenresExistInDatabase(): bool
    {
        return Genre::count() > 0;
    }

}
