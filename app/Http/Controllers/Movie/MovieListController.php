<?php

namespace App\Http\Controllers\Movie;

use App\Services\TMDB\TrendingMovieService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class MovieListController extends MovieController
{

    public function listDay(Request $request): View
    {
        return view('movie.list', [
            'timeWindow' => TrendingMovieService::TIME_DAY,
            'search' => $request->input('search')
        ]);
    }


    public function listWeek(Request $request): View
    {
        return view('movie.list', [
            'timeWindow' => TrendingMovieService::TIME_WEEK,
            'search' => $request->input('search')
        ]);
    }

}
