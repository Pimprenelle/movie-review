<?php

namespace App\Http\Controllers\Movie;

use App\Http\Controllers\Controller;
use App\Services\Genre\InitGenreService;
use App\Services\Genre\InitTrendingMovieService;
use App\Services\TMDB\TrendingMovieService;

abstract class MovieController extends Controller
{

    public function __construct(
        InitGenreService         $initGenreService,
        InitTrendingMovieService $initTrendingMovieService
    )
    {
        // Initialisation des genres dans l'application
        $initGenreService->createGenresFromApiIfNotExists();

        // Init/Rafraichissement des films trendings (1 fois par jour max)
        $initTrendingMovieService->refreshMoviesFromApi(TrendingMovieService::TIME_DAY);
        $initTrendingMovieService->refreshMoviesFromApi(TrendingMovieService::TIME_WEEK);
    }

}
