<?php

namespace App\Http\Controllers\Movie;

use App\Models\Genre;
use App\Models\Movie;
use Illuminate\Contracts\View\View;

class MovieDetailController extends MovieController
{

    public function detail(int $id): View
    {
        $movie = Movie::find($id);

        if (!$movie) {
            abort(404, 'Movie non trouvé');
        }
        return view('movie.detail', ['movie' => $movie]);
    }

}
