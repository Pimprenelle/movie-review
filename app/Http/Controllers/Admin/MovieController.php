<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Genre;
use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{

    public function index()
    {
        $movies = Movie::all();

        return view('admin.movie.index', compact('movies'));
    }

    public function show(Movie $movie)
    {
        return view('admin.movie.show', compact('movie'));
    }

    public function edit(Movie $movie)
    {

        $genres = Genre::all();

        return view('admin.movie.edit', compact('movie', 'genres'));
    }

    public function update(Request $request, Movie $movie)
    {

        $request->validate([
            'title' => 'required',
        ]);

        $movie->update($request->all());

        $movie->genres()->sync($request->input('genres'));

        return redirect()->route('admin.movies.index');
    }

    public function destroy(Movie $movie)
    {
        $movie->delete();

        return redirect()->route('admin.movies.index');
    }

}
