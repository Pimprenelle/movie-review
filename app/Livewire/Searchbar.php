<?php

namespace App\Livewire;

use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class Searchbar extends Component
{

    public $timeWindow;

    public function mount(string $timeWindow){
        $this->timeWindow = $timeWindow;
    }

    public function render()
    {
        return view('livewire.searchbar', [
            'timeWindow' => $this->timeWindow
        ]);
    }
}
