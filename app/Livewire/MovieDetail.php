<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Movie;

class MovieDetail extends Component
{

    public Movie $movie;

    public function mount(Movie $movie)
    {
        $this->movie = $movie;
    }

    public function render()
    {
        return view('livewire.movie-detail', [
            'movie' => $this->movie
        ]);
    }
}
