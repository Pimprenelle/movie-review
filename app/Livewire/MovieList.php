<?php

namespace App\Livewire;

use App\Models\Movie;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class MovieList extends Component
{

    use WithPagination;

    public $timeWindow;
    public $search;

    public function mount(
        string $timeWindow,
        ?string $search
    )
    {
        $this->timeWindow = $timeWindow;
        $this->search = $search;
    }

    public function render()
    {
        $today = Carbon::today();

        $movies = Movie::with('genres')
            ->when($this->search, function ($query) {
                if(strlen($this->search) > 0){
                    $query->where('title', 'like', '%' . $this->search . '%')
                        ->orWhere('original_title', 'like', '%' . $this->search . '%');
                }
            })
            ->whereDate("refresh_trending_" . $this->timeWindow . "_date", $today)
            ->orderBy('popularity', 'desc')
            ->paginate(8);

        return view('livewire.movie-list', [
            'movies' => $movies
        ]);
    }
}
