<?php

namespace App\Livewire;

use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class Genre extends Component
{

    public Collection $genres;

    public function mount(Collection $genres)
    {
        $this->genres = $genres;
    }

    public function render()
    {
        return view('livewire.genre');
    }
}
