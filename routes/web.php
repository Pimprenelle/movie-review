<?php

use App\Http\Controllers\Admin\GenreController;
use App\Http\Controllers\Admin\MovieController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Movie\MovieDetailController;
use App\Http\Controllers\Movie\MovieListController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [Controller::class, 'home'])
    ->name('home');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {

    Route::prefix('admin')->name('admin.')->group(function () {
        Route::get('/dashboard', function () {
            return view('dashboard');
        })->name('dashboard');

        Route::resource('genres', GenreController::class);
        Route::resource('movies', MovieController::class);

    });

});

Route::get('/movies/day', [MovieListController::class, 'listDay'])
    ->name('movie-list-day');

Route::get('/movies/week', [MovieListController::class, 'listWeek'])
    ->name('movie-list-week');

Route::get('/movies/{id}', [MovieDetailController::class, 'detail'])
    ->name('movie-detail')
    ->where('id', '[0-9]+');

Route::permanentRedirect('/admin', '/admin/movies');
Route::permanentRedirect('/admin/dashboard', '/admin/movies');
