# Movie Review

## Prérequis
- Docker et Docker-compose doivent être installés sur votre machine.

## Préparer l'environnement
### Exécution du Script

Pour automatiser le processus de préparation de l'environnement,
un script bash est fourni dans le dossier `bin`. Suivez les étapes ci-dessous :

1. **Donner les droits d'exécution au script :**
   ```bash
   chmod +x bin/prepare.sh
   ```

2. **Lancer le script :**
   ```bash
   ./bin/prepare.sh
   ```

3. Ajouter la clé d'API de TMDB dans la variable TMDB_API_KEY
    ```dotenv
    TMDB_API_KEY="ey......."
    ```


### Détail du script

Vous pouvez

1. **Lancer les conteneurs (laravel + mysql) :**
    ```bash
    ./vendor/bin/sail up -d
    ```

2. **Accéder au conteneur Laravel :**
    ```bash
    docker exec -it laravel.movie-review bash
    ```

3. **Dans conteneur : installer les dépendances :**
    ```bash
    composer install
    ```

4. **Dans conteneur : jouer les migrations :**
    ```bash
    php artisan migrate
    ```

5. **Dans conteneur : jouer les seeds pour avoir l'utilisateur admin :**
    ```bash
    php artisan db:seed
    ```

6. ** Copier le fichier .env.example en .env
    ```bash
    cp .env.example .env
    ```


## Utilisation
Une fois que l'environnement est préparé, vous pouvez accéder à l'application en ouvrant votre navigateur et en visitant [http://localhost:8890](http://localhost:8890). Pour accéder au backoffice, utilisez les informations d'identification suivantes :
- **Email :** a@a.com
- **Mot de passe :** admin

## Fonctionnalités
- **Backoffice restreint :** L'application utilise BackPack pour créer un backoffice avec un accès restreint. Vous pouvez vous connecter en utilisant les informations d'identification fournies ci-dessus.
- **Liste des films tendances :** Accédez à la page d'accueil pour voir la liste des derniers films tendances du jour ou du mois.
- **Visualisation du contenu d'un film :** Cliquez sur un film pour voir plus de détails, y compris le nom, la description, l'image et d'autres informations.
- **Dockerisé avec Sail :** Le projet est dockerisé à l'aide de Sail pour une gestion facile des conteneurs.
- **Optimisation des appels API:** Les données de l'API sont récupérées et stockées dans la base de données pour optimiser le fonctionnement du backoffice. Une seule fois par jour au maximum.

## Commandes Artisan
- **Initialisation :** Pour récupérer les genres via l'API des films de TMDB, utilisez la commande artisan suivante :
    ```bash
    php artisan api:get-genres
    ```
  Cette commande peut également être exécutée automatiquement en allant sur la page ne contenant aucune donnée ou en appuyant sur le bouton de rafraîchissement.

  Elle ne produit un résultat que s'il n'y a pas de genre en BDD.


- **Mise à jour manuelle des données :** Pour mettre à jour manuellement les données de l'API, utilisez la commande artisan suivante :
    ```bash
    # Pour les films tendances du jour
    php artisan api:get-movies --day
  
    # Pour les films tendances du weekend
    php artisan api:get-movies --week
    ```
  Cette commande peut également être exécutée automatiquement en allant sur la page ne contenant aucune donnée ou en appuyant sur le bouton de rafraîchissement, avec une limitation d'une fois par jour.


## Backup de la Base de Données
Si les commandes Artisan ne sont pas possibles, vous pouvez utiliser le fichier de sauvegarde de la base de données de développement au format .sql fourni dans le dossier `backup`.


---

N'hésitez pas à me contacter pour toute question ou assistance. Bonne utilisation !
