-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: movie_review
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `genre_movie`
--

DROP TABLE IF EXISTS `genre_movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genre_movie` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `genre_id` bigint unsigned NOT NULL,
  `movie_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genre_movie_genre_id_movie_id_unique` (`genre_id`,`movie_id`),
  KEY `genre_movie_movie_id_foreign` (`movie_id`),
  CONSTRAINT `genre_movie_genre_id_foreign` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`) ON DELETE CASCADE,
  CONSTRAINT `genre_movie_movie_id_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre_movie`
--

LOCK TABLES `genre_movie` WRITE;
/*!40000 ALTER TABLE `genre_movie` DISABLE KEYS */;
INSERT INTO `genre_movie` VALUES (3,4,2,NULL,NULL),(4,14,2,NULL,NULL),(5,15,2,NULL,NULL),(6,1,3,NULL,NULL),(7,2,3,NULL,NULL),(8,15,3,NULL,NULL),(9,1,4,NULL,NULL),(10,5,4,NULL,NULL),(11,15,4,NULL,NULL),(12,2,5,NULL,NULL),(13,15,5,NULL,NULL),(14,4,6,NULL,NULL),(15,11,6,NULL,NULL),(16,14,6,NULL,NULL),(17,4,7,NULL,NULL),(18,8,7,NULL,NULL),(19,9,7,NULL,NULL),(20,4,8,NULL,NULL),(21,7,8,NULL,NULL),(22,17,8,NULL,NULL),(23,4,9,NULL,NULL),(24,7,9,NULL,NULL),(25,14,9,NULL,NULL),(26,4,10,NULL,NULL),(27,7,11,NULL,NULL),(28,10,11,NULL,NULL),(29,18,11,NULL,NULL),(30,4,12,NULL,NULL),(31,14,12,NULL,NULL),(32,7,13,NULL,NULL),(33,10,13,NULL,NULL),(34,11,14,NULL,NULL),(35,17,14,NULL,NULL),(36,1,15,NULL,NULL),(37,2,15,NULL,NULL),(38,3,15,NULL,NULL),(39,4,15,NULL,NULL),(40,8,15,NULL,NULL),(41,11,16,NULL,NULL),(42,13,16,NULL,NULL),(43,17,16,NULL,NULL),(44,7,17,NULL,NULL),(45,9,17,NULL,NULL),(46,14,17,NULL,NULL),(47,1,18,NULL,NULL),(48,2,18,NULL,NULL),(49,15,18,NULL,NULL),(50,1,19,NULL,NULL),(51,17,19,NULL,NULL),(52,18,19,NULL,NULL),(53,1,20,NULL,NULL),(54,9,20,NULL,NULL),(55,15,20,NULL,NULL),(56,1,21,NULL,NULL),(57,2,21,NULL,NULL),(58,9,21,NULL,NULL),(59,1,22,NULL,NULL),(60,7,22,NULL,NULL),(61,17,22,NULL,NULL),(62,7,23,NULL,NULL),(63,10,23,NULL,NULL),(64,4,24,NULL,NULL),(65,7,24,NULL,NULL),(66,2,25,NULL,NULL),(67,4,25,NULL,NULL),(68,1,2,NULL,NULL);
/*!40000 ALTER TABLE `genre_movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genres` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmdb_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
INSERT INTO `genres` VALUES (1,'Action',28,'2024-02-29 11:42:18','2024-02-29 14:48:22'),(2,'Aventure.',12,'2024-02-29 11:42:18','2024-02-29 15:57:58'),(3,'Animation',16,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(4,'Comédie',35,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(5,'Crime',80,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(6,'Documentaire',99,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(7,'Drame',18,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(8,'Familial',10751,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(9,'Fantastique',14,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(10,'Histoire',36,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(11,'Horreur',27,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(12,'Musique',10402,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(13,'Mystère',9648,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(14,'Romance',10749,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(15,'Science-Fiction',878,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(16,'Téléfilm',10770,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(17,'Thriller',53,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(18,'Guerre',10752,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(19,'Western',37,'2024-02-29 11:42:18','2024-02-29 11:42:18');
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (10,'2014_10_12_000000_create_users_table',1),(11,'2014_10_12_100000_create_password_reset_tokens_table',1),(12,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(13,'2019_08_19_000000_create_failed_jobs_table',1),(14,'2019_12_14_000001_create_personal_access_tokens_table',1),(15,'2024_02_27_180512_create_sessions_table',1),(16,'2024_02_27_210954_create_genres_table',1),(17,'2024_02_28_085237_create_movies_table',1),(18,'2024_02_28_085550_create_genre_movie_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `adult` tinyint(1) DEFAULT NULL,
  `backdrop_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmdb_id` int NOT NULL,
  `original_language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overview` text COLLATE utf8mb4_unicode_ci,
  `popularity` double(8,2) DEFAULT NULL,
  `poster_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vote_average` double(8,2) DEFAULT NULL,
  `vote_count` int DEFAULT NULL,
  `refresh_trending_day_date` date DEFAULT NULL,
  `refresh_trending_week_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies`
--

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` VALUES (2,0,'/jp5MBWKaDVBuzaSFHTHGc9kpu51.jpg',792307,'en','Poor Things','Une fable étonnante ayant pour héroïne Bella Baxter, une jeune femme ramenée à la vie par un savant brillant et peu conventionnel. Désireuse de connaître le monde, Bella s\'embarque dans une aventure tourbillonnante sur plusieurs continents. Étrangère aux préjugés de son époque, Bella fonce tête baissée dans sa quête d\'égalité et d\'émancipation.',1202.11,'/5fT98da9ccWN2xr8VOJrSBp3Cdw.jpg','2023-12-07','Pauvres créatures',8.11,1465,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(3,0,'/qBQV4i2sjIydDJUKm7pwvpyfy5p.jpg',693134,'en','Dune: Part Two','Paul Atréides se rallie à Chani et aux Fremen tout en préparant sa revanche contre ceux qui ont détruit sa famille. Alors qu\'il doit faire un choix entre l\'amour de sa vie et le destin de la galaxie, il devra néanmoins tout faire pour empêcher un terrible futur que lui seul peut prédire.',968.34,'/rBrJl1vUtaM6NU0EwxKSnc3nBxE.jpg','2024-02-27','Dune - Deuxième partie',8.46,147,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(4,0,'/n9NMlYd4TrSPErkRctNJaeTkGCM.jpg',461130,'en','Code 8','Dans un monde où 4% de la population est née avec des pouvoirs surnaturels, où les personnes étant dotées de ces capacités sont pauvres et discriminées, un homme se bat pour payer le traitement médical de sa mère. Il est bientôt attiré par un univers criminel lucratif dans lequel ses pouvoirs seront très utiles…',65.27,'/zINIqjLjRtFkbc6ELeUHiKCXtaS.jpg','2019-12-06','Code 8',6.20,1666,'2024-02-29',NULL,'2024-02-29 11:42:18','2024-02-29 15:57:18'),(5,0,'/lzWHmYdfeFiMIY4JaMmtR7GEli3.jpg',438631,'en','Dune','L\'histoire de Paul Atreides, jeune homme aussi doué que brillant, voué à connaître un destin hors du commun qui le dépasse totalement. Car, s\'il veut préserver l\'avenir de sa famille et de son peuple, il devra se rendre sur Dune, la planète la plus dangereuse de l\'Univers. Mais aussi la seule à même de fournir la ressource la plus précieuse capable de décupler la puissance de l\'Humanité. Tandis que des forces maléfiques se disputent le contrôle de cette planète, seuls ceux qui parviennent à dominer leur peur pourront survivre…',714.38,'/qpyaW4xUPeIiYA5ckg5zAZFHvsb.jpg','2021-09-15','Dune - Première partie',7.79,10244,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(6,0,'/tsDoiLwZyAbYewoIUDJKk1SJzTF.jpg',993784,'en','Lisa Frankenstein','Une histoire d\'amour et d\'horreur de la scénariste acclamée Diablo Cody à propos d\'une adolescente incomprise et de son petit ami, un charmant cadavre. Après que des circonstances horriblement amusantes ont ramené ce dernier à la vie, les deux amoureux entament une aventure meurtrière à la poursuite de l\'amour, du bonheur… et de quelques membres manquants en chemin.',86.37,'/mdPnNWU05ivcVp0rF2Dv9n5Maf4.jpg','2024-02-07','Lisa Frankenstein',6.30,45,'2024-02-29',NULL,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(7,0,'/yOm993lsJyPmBodlYjgpPwBjXP9.jpg',787699,'en','Wonka','Inspiré du personnage principal hors du commun de Charlie et la Chocolaterie, le best-seller de Roald Dahl qui est aujourd\'hui l\'un des livres pour enfants les plus vendus de tous les temps, \"Wonka\" est une histoire merveilleuse retraçant la jeunesse de Willy Wonka, et comment il est devenu ce grand inventeur, magicien et chocolatier que nous connaissons aujourd\'hui. Dans ce spectacle saisissant et d\'une créativité sans limites, les spectateurs découvriront un Willy Wonka jeune, la tête débordant d\'idées et bien décidé à changer le monde à coups de délicieuses chocolateries. Il vous montrera que les meilleures choses de la vie commencent par un rêve, et qu\'en rencontrant Willy Wonka, tout devient possible.',736.30,'/aKAnL3sr5iz5SsOs89vS2k1XrKO.jpg','2023-12-06','Wonka',7.21,2311,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(8,0,'/hiKmpZMGZsrkA3cdce8a7Dpos1j.jpg',496243,'ko','기생충','Toute la famille de Ki-taek est au chômage. Elle s’intéresse particulièrement au train de vie de la richissime famille Park. Mais un incident se produit et les deux familles se retrouvent mêlées, sans le savoir, à une bien étrange histoire…',184.33,'/7hLSzZX2jROmEXz2aEoh6JKUFy2.jpg','2019-05-30','Parasite',8.51,17185,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(9,0,'/iWzu3dVJGNfQUkLfK9xpTWoi5mh.jpg',1139566,'es','A través de mi ventana 3: A través de tu mirada','Raquel et Ares font face aux défis qui se présentent dans ce dernier chapitre de leur relation et de leurs vies individuelles.',747.68,'/maJNyAG2WLHfoigbYZsGznK7KSM.jpg','2024-02-23','À travers ma fenêtre 3 : Les yeux dans les yeux',6.64,123,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(10,0,'/dmiN2rakG9hZW04Xx7mHOoHTOyD.jpg',673593,'en','Mean Girls','La nouvelle élève Cady Heron est accueillie au sommet de la hiérarchie sociale par le groupe des filles populaires appelé « Les Plastiques », dirigé par l\'impitoyable diva, Regina George. Après avoir conclu une alliance secrète avec les exclus de l\'école pour renverser Regina, Cady doit apprendre à rester fidèle à elle-même tout en naviguant dans la jungle la plus impitoyable de toutes : le lycée.',420.29,'/5F4YY8EILBseITy1AsvwFhTqTC0.jpg','2024-01-10','Mean Girls : Lolita Malgré Moi',6.41,192,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(11,0,'/aPQsU3yLDUOhLJYnSqkhKRkQTAw.jpg',467244,'en','The Zone of Interest','Le commandant d\'Auschwitz Rudolf Höss et son épouse Hedwig réalisent sur un terrain directement adjacent au mur du camp leur vision d\'une vie de rêve avec une famille nombreuse, une maison et un grand jardin. Cependant, lorsque Rudolf doit être muté à Oranienburg, leur petite vie idéale menace de s\'effondrer et il cache l\'information à son épouse. Quand Hedwig l\'apprend, elle refuse de quitter sa maison de rêve.',212.98,'/wD2eFNkSLV0ACM9C82qIYfTMFv9.jpg','2023-12-15','La Zone d\'intérêt',7.36,479,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(12,0,'/nTPFkLUARmo1bYHfkfdNpRKgEOs.jpg',1072790,'en','Anyone But You','Bea et Ben ont tout du couple parfait, mais après un premier rendez-vous idéal, un incident refroidit leur attirance réciproque jusqu\'à leurs retrouvailles inattendues lors d\'un mariage en Australie. Ils font alors ce que n\'importe quel adulte mature ferait dans cette situation : prétendre être en couple.',1248.64,'/41Dg2A3EsS3mAjeqVH1c2EdeGU3.jpg','2023-12-21','Tout sauf toi',6.91,691,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(13,0,'/fm6KqXpk3M2HVveHwCrBSSBaO0V.jpg',872585,'en','Oppenheimer','En 1942, convaincus que l\'Allemagne nazie est en train de développer une arme nucléaire, les États-Unis initient, dans le plus grand secret, le \"Projet Manhattan\" destiné à mettre au point la première bombe atomique de l’histoire. Pour piloter ce dispositif, le gouvernement engage J. Robert Oppenheimer, brillant physicien, qui sera bientôt surnommé \"le père de la bombe atomique\". C\'est dans le laboratoire ultra-secret de Los Alamos, au cœur du désert du Nouveau-Mexique, que le scientifique et son équipe mettent au point une arme révolutionnaire dont les conséquences, vertigineuses, continuent de peser sur le monde actuel…',377.09,'/boAUuJBeID7VNp4L7LNMQs8mfQS.jpg','2023-07-19','Oppenheimer',8.10,6879,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(14,0,'/jCyNSPcHTIfXfxF7Tf5yJxCKpog.jpg',823491,'en','Out of Darkness','Les premiers humains se regroupent à la recherche d\'une nouvelle terre. Lorsqu\'ils soupçonnent un être malveillant de les pourchasser, le clan est contraint de faire face à un danger effroyable qu\'ils n\'avaient jamais imaginé.',27.67,'/9tG77VE7bP8ve79X86ABwHzDqbb.jpg','2024-02-09','Out of Darkness',5.67,3,'2024-02-29',NULL,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(15,0,'/meyhnvssZOPPjud4F1CjOb4snET.jpg',940551,'en','Migration','La routine règne chez les Colvert. Si papa Mack préfère garder les siens bien à l’abri dans leur étang de la Nouvelle-Angleterre, maman Pam veut changer les choses et faire découvrir le monde à leurs deux enfants, Dax l’ado et Gwen la cadette. Après qu’une famille de canards migrateurs s’est enflammée pour des récits parlant d’endroits très lointains, Pam persuade Mack d’embarquer la famille dans un long périple, depuis la ville de New York jusque sous les tropiques, en Jamaïque. Tandis que les Colvert s’envolent vers le Sud pour l’hiver, leurs projets bien rodés tombent rapidement à l’eau. Une expérience qui les pousse alors à étendre leurs horizons, à s’ouvrir à de nouveaux amis et à aller au-delà de leurs espoirs les plus fous, tout en apprenant davantage sur chacun et sur soi-même',1009.99,'/z41CiKh09ljWXqHoGNb4zLom03K.jpg','2023-12-06','Migration',7.63,793,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(16,0,'/aINel9503ompOlGKn4sIVMg09Un.jpg',838209,'ko','파묘','Une famille aisée vivant à Los Angeles fait appel à deux jeunes chamans pour sauver leur nouveau-né après avoir été victime d\'une série d\'événements paranormaux.',164.58,'/pQYHouPsDw32FhDLr7E3jmw0WTk.jpg','2024-02-22','Exhuma',8.60,4,'2024-02-29',NULL,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(17,0,'/2GzgongTSptjSkT7iCoXUcGIVB9.jpg',994108,'en','All of Us Strangers','Le scénariste Adam rencontre son mystérieux voisin Harry. Alors qu\'Adam et Harry se rapprochent, Adam est ramené dans sa maison d\'enfance où il découvre que ses parents - décédés depuis longtemps - sont tous deux vivants et semblent avoir le même âge que le jour de leur mort, il y a plus de 30 ans.',187.61,'/8jTbE3u4geKS08zQ49nL0xwSJoH.jpg','2023-12-22','Sans jamais nous connaître',7.51,184,'2024-02-29',NULL,'2024-02-29 11:42:18','2024-02-29 11:42:18'),(18,0,'/feSiISwgEpVzR1v3zv2n2AU4ANJ.jpg',609681,'en','The Marvels','Carol Danvers, alias Captain Marvel, a récupéré son identité auprès du tyrannique Kree et s\'est vengée du renseignement suprême. Cependant, des conséquences inattendues la voient assumer le fardeau d\'un univers déstabilisé. Lorsque ses fonctions l\'envoient dans un trou de ver anormal lié à un révolutionnaire Kree, ses pouvoirs s\'entremêlent avec deux autres superhéros pour former les Marvels.',767.52,'/2XS0jH2F4SFiJG7txgWjgjwJ2L8.jpg','2023-11-08','The Marvels',6.25,1917,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(19,0,'/oBIQDKcqNxKckjugtmzpIIOgoc4.jpg',969492,'en','Land of Bad','Le pilote de drone de l\'armée de l\'air, Reaper, est engagé dans une mission d\'opérations spéciales de la Delta Force dans le sud des Philippines mais la mission tourne mal...',1740.93,'/z1uj3ZGvMa03gqYsl1jns0CBy8z.jpg','2024-01-25','Land of Bad',7.02,207,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(20,0,'/uUiIGztTrfDhPdAFJpr6m4UBMAd.jpg',634492,'en','Madame Web','Cassandra Web est une ambulancière de Manhattan qui serait capable de voir dans le futur. Forcée de faire face à des révélations sur son passé, elle noue une relation avec trois jeunes femmes destinées à un avenir hors du commun... si toutefois elles parviennent à survivre à un présent mortel.',326.38,'/rULWuutDcN5NvtiZi4FRPzRYWSh.jpg','2024-02-14','Madame Web',5.39,276,'2024-02-29','2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(21,0,'/bckxSN9ueOgm0gJpVJmPQrecWul.jpg',572802,'en','Aquaman and the Lost Kingdom','Alors qu\'Arthur Curry affronte ses responsabilités de Roi des Sept Mers, un pouvoir antique enfoui depuis longtemps se déchaîne. Après avoir été témoin de l\'effet de ces forces obscures, Aquaman doit forger une alliance difficile avec un vieil ennemi et se lancer dans un voyage périlleux pour protéger sa famille, son royaume et le monde d\'une dévastation irréversible.',562.47,'/w8r7NAEIGLPH5r3NhiMobEO80PS.jpg','2023-12-20','Aquaman et le Royaume perdu',6.87,1841,NULL,'2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(22,0,'/4MCKNAc6AbWjEsM2h9Xc29owo4z.jpg',866398,'en','The Beekeeper','La quête brutale de vengeance d’un homme prend des proportions démesurées alors que son passé d’agent secret d’une puissante organisation clandestine connue sous le nom des Apiculteurs est révélé.',405.11,'/oJWHzabe1QxgomK4gaEDhITvxcM.jpg','2024-01-10','The Beekeeper',7.40,1436,NULL,'2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(23,0,'/4l65BWqJBl7hBwdIwp2nQdwsOuw.jpg',850165,'en','The Iron Claw','L\'histoire vraie des inséparables frères Von Erich, qui ont marqué l\'histoire du monde extrêmement compétitif de la lutte professionnelle au début des années 1980. À travers la tragédie et le triomphe, sous l\'ombre de leur père et entraîneur tyrannique, les frères recherchent une immortalité plus grande que nature sur la plus grande scène sportive.',111.02,'/3kTL4nIXJHzXqkjPWqytmcmwyM7.jpg','2023-12-21','Iron Claw',7.53,299,NULL,'2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(24,0,'/3mpgltEMgPf8zFtPnAWdDVN8ZT1.jpg',1056360,'en','American Fiction','Monk est un romancier frustré qui en a assez de l\'establishment qui profite du divertissement noir qui s\'appuie sur des tropes fatigués et offensants. Pour prouver son point de vue, il utilise un pseudonyme pour écrire son propre livre noir extravagant, un livre qui le propulse au cœur de l\'hypocrisie et de la folie qu\'il prétend dédaigner.',105.96,'/eTyxDvtOpmTWy7rlXXCAhLsXhEP.jpg','2023-11-10','American Fiction',7.29,269,NULL,'2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18'),(25,0,'/ctMserH8g2SeOAnCw5gFjdQF8mo.jpg',346698,'en','Barbie','Parallèlement au monde réel, il existe Barbie Land, un monde parfait où les poupées Barbie vivent joyeusement, persuadées d\'avoir rendu les filles humaines heureuses. Mais un jour, une Barbie commence à se poser des questions et à devenir humaine. Sur les conseils d\'une Barbie étrange, elle part pour le monde réel afin de retrouver la fille à laquelle elle appartenait afin de pouvoir retrouver sa perfection. Dans sa quête, elle est accompagnée par un Ken fou amoureux d\'elle qui va également trouver un sens à sa vie dans le monde réel…',304.74,'/2EXE5WolOOrIqCmWuL6eOQG8Qxy.jpg','2023-07-19','Barbie',7.10,7535,NULL,'2024-02-29','2024-02-29 11:42:18','2024-02-29 11:42:18');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `two_factor_confirmed_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint unsigned DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','a@a.com',NULL,'$2y$12$X5f5tkprk57PUTVjsH0toOCJfmK0hc6xLRpJsYDumsO/0pA0bBMNm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-29 16:06:02
