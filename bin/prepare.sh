#!/bin/bash

echo "Lancement des conteneurs (laravel + mysql)..."
./vendor/bin/sail up -d

echo "Installation des dépendances avec composer..."
docker exec -it laravel.movie-review bash -c "cd /var/www/html && composer install"

echo "Exécution des migrations..."
docker exec -it laravel.movie-review bash -c "cd /var/www/html && php artisan migrate"

echo "Exécution du seed pour créer l'utilisateur admin..."
docker exec -it laravel.movie-review bash -c "cd /var/www/html && php artisan db:seed"

echo "Clear all caches"
docker exec -it laravel.movie-review bash -c "cd /var/www/html && php artisan cache:clear"
docker exec -it laravel.movie-review bash -c "cd /var/www/html && php artisan view:clear"
docker exec -it laravel.movie-review bash -c "cd /var/www/html && php artisan config:clear"
docker exec -it laravel.movie-review bash -c "cd /var/www/html && php artisan route:clear"

echo -e "\n\n"

echo "En résumé: "
echo "- Lancement des conteneurs (laravel + mysql)"
echo "- Installation des dépendances avec composer"
echo "- Exécution des migrations"
echo "- Exécution du seed pour créer l'utilisateur admin"
echo "- Clear all caches"


echo -e "\n\033[1;32m***********************************************"
echo -e "Le site est maintenant disponible ici: http://localhost:8890"
echo -e "N'oubliez pas de copier le fichier .env.example en .env et renseignez votre clé API"
echo -e "***********************************************\033[0m\n"
