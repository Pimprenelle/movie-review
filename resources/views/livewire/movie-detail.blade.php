<div class="row gx-4 gx-lg-5 align-items-center">
    <div class="col-md-4"><img class="card-img-top mb-5 mb-md-0" src="https://media.themoviedb.org/t/p/w220_and_h330_face{{ $movie['poster_path'] }}" alt="{{ $movie['title'] }}" /></div>
    <div class="col-md-8">
        <div class="small mb-1">[{{ strtoupper($movie['original_language']) }}] {{ $movie['original_title'] }}</div>
        <h1 class="display-5 fw-bolder">{{ $movie['title'] }}</h1>

        <div>
            @foreach ($movie->genres()->get() as $genre)
                <span class="badge bg-primary">{{ $genre->name }}</span>
            @endforeach
        </div>

        <div class="d-flex  small text-warning mb-2">

            @php
                $rating = $movie['vote_average'];
                $starsCount = floor($rating / 2);
            @endphp

            @for($i = 0; $i < 5; $i++)
                @if($i < $starsCount)
                    <div class="bi-star-fill"></div>
                @else
                    <div class="bi-star"></div>
                @endif
            @endfor

        </div>
        <div class="fs-5 mb-5">
            <span>Sortie le {{ $movie['release_date'] }}</span>
        </div>
        <p class="lead">{{ $movie['overview'] }}</p>
        <div class="d-flex">
            <button class="btn btn-outline-dark flex-shrink-0" type="button" onclick="history.back()">
             Retour
            </button>
        </div>
    </div>
</div>
