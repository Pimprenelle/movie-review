<div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">

    @foreach ($movies as $movie)
        <div class="col mb-5">
            <div class="card h-100">
                <!-- Product image-->
                <img class="card-img-top"
                     src="https://media.themoviedb.org/t/p/w220_and_h330_face{{ $movie->poster_path }}"
                     alt="{{ $movie->title }}"
                     onerror="this.onerror=null;this.src='https://dummyimage.com/268x402/dee2e6/6c757d.jpg';"
                />
                <!-- Product details-->
                <div class="card-body p-4">
                    <div class="text-center">
                        <!-- Product name-->
                        <h5 class="fw-bolder">{{ $movie->title }}</h5>

                        <div>
                            @foreach ($movie->genres()->get() as $genre)
                                <span class="badge bg-primary">{{ $genre->name }}</span>
                            @endforeach
                        </div>

                        <div class="d-flex justify-content-center small text-warning mb-2">

                            @php
                                $rating = $movie->vote_average;
                                $starsCount = floor($rating / 2);
                            @endphp

                            @for($i = 0; $i < 5; $i++)
                                @if($i < $starsCount)
                                    <div class="bi-star-fill"></div>
                                @else
                                    <div class="bi-star"></div>
                                @endif
                            @endfor

                        </div>

                        Sortie le {{ $movie->release_date }}

                    </div>
                </div>
                <!-- Product actions-->
                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div class="text-center">
                        <a class="btn btn-outline-dark mt-auto"
                           href="{{ route('movie-detail', ['id' => $movie->id]) }}">Detail</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    {{ $movies->links() }}
</div>

