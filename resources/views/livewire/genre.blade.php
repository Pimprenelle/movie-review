<div>
    <h1>Liste des genres</h1>

    <ul>
        @foreach ($genres as $genre)
            <li>{{ $genre['name'] }}</li>
        @endforeach
    </ul>
</div>
