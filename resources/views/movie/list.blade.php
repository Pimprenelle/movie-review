@extends('layouts.custom')

@section('header')
    True
@endsection

@section('title')
    @if($timeWindow === App\Services\TMDB\TrendingMovieService::TIME_WEEK)
        Trending de la semaine
    @else
        Trending du jour
    @endif
@endsection

@section('content')
    @livewire('searchbar', [
    'timeWindow' => $timeWindow,
    ])

    @livewire('movie-list', [
    'timeWindow' => $timeWindow,
    'search' => $search
    ])
@endsection
