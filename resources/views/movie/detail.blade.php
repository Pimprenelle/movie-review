@extends('layouts.custom')


@section('title')
    {{ $movie->title }}
@endsection

@section('content')
    @livewire('movie-detail', ['movie' => $movie])
@endsection
