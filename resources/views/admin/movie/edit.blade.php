<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Editer un Film
        </h2>
    </x-slot>

    <div>
        <div class="max-w-4xl mx-auto py-10 sm:px-6 lg:px-8">

            <div class="mt-5 md:mt-0 md:col-span-2">
                <form method="post" action="{{ route('admin.movies.update', $movie->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="shadow overflow-hidden sm:rounded-md">
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="name" class="required block font-medium text-sm text-gray-700">Title</label>
                            <input type="text" name="title" id="title" type="text" required="required"
                                   class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('title', $movie->title) }}" />
                            @error('title')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="genres" class="block font-medium text-sm text-gray-700">Genres</label>
                            <select name="genres[]" id="genres" class="form-input rounded-md shadow-sm mt-1 block w-full" multiple>
                                @foreach ($genres as $genre)
                                    <option value="{{ $genre->id }}"
                                        {{ $movie->genres->contains($genre) ? 'selected' : '' }}>
                                        {{ $genre->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="tmdb_id" class="block font-medium text-sm text-gray-700">tmdb_id</label>
                            <input disabled="disabled" type="text" name="tmdb_id" id="tmdb_id" type="text" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('tmdb_id', $movie->tmdb_id) }}" />
                            @error('tmdb_id')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="adult" class="block font-medium text-sm text-gray-700">Adult</label>
                            <input type="checkbox" name="adult" id="adult" class="form-checkbox h-5 w-5 text-indigo-600"
                                {{ $movie->adult ? 'checked' : '' }}>
                            @error('adult')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="backdrop_path" class="block font-medium text-sm text-gray-700">Backdrop Path</label>
                            <input type="text" name="backdrop_path" id="backdrop_path" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('backdrop_path', $movie->backdrop_path) }}" />
                            @error('backdrop_path')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="original_language" class="block font-medium text-sm text-gray-700">Original Language</label>
                            <input type="text" name="original_language" id="original_language" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('original_language', $movie->original_language) }}" />
                            @error('original_language')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="original_title" class="block font-medium text-sm text-gray-700">Original Title</label>
                            <input type="text" name="original_title" id="original_title" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('original_title', $movie->original_title) }}" />
                            @error('original_title')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="overview" class="block font-medium text-sm text-gray-700">Overview</label>
                            <textarea name="overview" id="overview" class="form-input rounded-md shadow-sm mt-1 block w-full">{{ old('overview', $movie->overview) }}</textarea>
                            @error('overview')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="poster_path" class="block font-medium text-sm text-gray-700">Poster Path</label>
                            <input type="text" name="poster_path" id="poster_path" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('poster_path', $movie->poster_path) }}" />
                            @error('poster_path')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="popularity" class="block font-medium text-sm text-gray-700">Popularity</label>
                            <input type="text" name="popularity" id="popularity" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('popularity', $movie->popularity) }}" />
                            @error('popularity')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="release_date" class="block font-medium text-sm text-gray-700">Release Date</label>
                            <input type="date" name="release_date" id="release_date" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('release_date', $movie->release_date?->format('Y-m-d')) }}" />
                            @error('release_date')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="vote_average" class="block font-medium text-sm text-gray-700">Vote Average</label>
                            <input type="text" name="vote_average" id="vote_average" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('vote_average', $movie->vote_average) }}" disabled>
                            @error('vote_average')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>




                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="vote_count" class="block font-medium text-sm text-gray-700">Vote Count</label>
                            <input disabled="disabled" type="number" name="vote_count" id="vote_count" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('vote_count', $movie->vote_count) }}" />
                            @error('vote_count')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="refresh_trending_day_date" class="block font-medium text-sm text-gray-700">Refresh Trending Day Date</label>
                            <input disabled="disabled" type="date" name="refresh_trending_day_date" id="refresh_trending_day_date" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('refresh_trending_day_date', $movie->refresh_trending_day_date?->format('Y-m-d')) }}" />
                            @error('refresh_trending_day_date')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="refresh_trending_week_date" class="block font-medium text-sm text-gray-700">Refresh Trending Week Date</label>
                            <input disabled="disabled" type="date" name="refresh_trending_week_date" id="refresh_trending_week_date" class="form-input rounded-md shadow-sm mt-1 block w-full"
                                   value="{{ old('refresh_trending_week_date', $movie->refresh_trending_week_date?->format('Y-m-d')) }}" />
                            @error('refresh_trending_week_date')
                            <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>



                        <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button
                                class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                Editer
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
