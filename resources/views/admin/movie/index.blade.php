<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Liste des Films
        </h2>
    </x-slot>

    <div>


        <div class="max-w-6xl mx-auto py-10 sm:px-6 lg:px-8">
            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="flex-auto block py-8 pt-6 px-9">
                        <div class="overflow-x-auto">
                            <table class="w-full my-0 align-middle text-dark border-neutral-200">
                                <thead class="align-bottom">
                                <tr class="font-semibold text-[0.95rem] text-secondary-dark">
                                    <th scope="col" class="pb-3 text-start">
                                        ID
                                    </th>
                                    <th scope="col" class="pb-3 text-start">
                                        Name
                                    </th>
                                    <th scope="col" class="pb-3 text-start">
                                        Date Last Trending Day Refresh
                                    </th>
                                    <th scope="col" class="pb-3 text-start">
                                        Date Last Trending Week Refresh
                                    </th>
                                    <th scope="col"  class="pb-3">

                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @foreach ($movies as $movie)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                            {{ $movie->id }}
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                            {{ $movie->title }}
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                            {{ $movie->refresh_trending_day_date?->format('d-m-Y')}}
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                            {{ $movie->refresh_trending_week_date?->format('d-m-Y')}}
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                            <a href="{{ route('admin.movies.show', $movie->id) }}" class="text-blue-600 hover:text-blue-900 mb-2 mr-2">Détail</a>
                                            <a href="{{ route('admin.movies.edit', $movie->id) }}" class="text-indigo-600 hover:text-indigo-900 mb-2 mr-2">Editer</a>
                                            <form class="inline-block" action="{{ route('admin.movies.destroy', $movie->id) }}" method="POST" onsubmit="return confirm('Etes-vous sûr ?');">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="submit" class="text-red-600 hover:text-red-900 mb-2 mr-2" value="Supprimer">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</x-app-layout>
